package br.edu.fatecsjc.aloprefeitura.repositories;

import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.fatecsjc.aloprefeitura.models.Setor;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;

public interface SetorRepository extends JpaRepository<Setor, Long> {
	Setor findById(Long id);
	
	List<Setor> findAll();
	
	@Null
	public Setor findByNome(String nome);
}
