package br.edu.fatecsjc.aloprefeitura.repositories;

import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.fatecsjc.aloprefeitura.models.Midia;
import br.edu.fatecsjc.aloprefeitura.models.Ocorrencia;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;

public interface MidiaRepository extends JpaRepository<Midia, Long> {
	
	Midia findById(Long id);
	
	List<Midia> findAll();
	
	@Null
	public Midia findByOcorrencia(Ocorrencia ocurr);
}
