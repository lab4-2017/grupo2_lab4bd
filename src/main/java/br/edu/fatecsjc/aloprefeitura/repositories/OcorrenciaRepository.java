package br.edu.fatecsjc.aloprefeitura.repositories;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.fatecsjc.aloprefeitura.models.Ocorrencia;
import br.edu.fatecsjc.aloprefeitura.models.StatusOcorrencia;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;

public interface OcorrenciaRepository extends JpaRepository<Ocorrencia, Long> {
	
	public Ocorrencia findById(Long id);
	
	public List<Ocorrencia> findAll();
	
	public List<Ocorrencia> findByStatusOcorrencia(StatusOcorrencia statusOcorrencia);
	
	public List<Ocorrencia> findByUsuario(Usuario usuario);
	
	public List<Ocorrencia> findByUsuarioAndStatusOcorrencia(Usuario usuario, StatusOcorrencia statusOcorrencia);

	@Query("select oc from Ocorrencia oc where oc.setor.id = :setId")
	public List<Ocorrencia> findBySetorId(@Param("setId")Long setId);
	
	@Query("select oc from Ocorrencia oc where oc.dataHora between :ini and :fim")
	public List<Ocorrencia> findByPeriodo(@Param("ini")Calendar ini, @Param("fim")Calendar fim);
	
}
