package br.edu.fatecsjc.aloprefeitura.repositories;

import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.fatecsjc.aloprefeitura.models.Setor;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	Usuario findById(Long id);
	
	List<Usuario> findAll();
	
	@Null
	public Usuario findByNome(String nome);
	
	@Null
	@Query("SELECT u FROM Usuario u WHERE u.setor.id = :setId")
	public List<Usuario> findBySetorId(@Param("setId")Long setId);
}
