package br.edu.fatecsjc.aloprefeitura.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.fatecsjc.aloprefeitura.exceptions.ApiException;
import br.edu.fatecsjc.aloprefeitura.models.Ocorrencia;
import br.edu.fatecsjc.aloprefeitura.models.StatusOcorrencia;
import br.edu.fatecsjc.aloprefeitura.models.TipoUsuario;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;
import br.edu.fatecsjc.aloprefeitura.repositories.OcorrenciaRepository;
import br.edu.fatecsjc.aloprefeitura.repositories.ReputacaoRepository;
import br.edu.fatecsjc.aloprefeitura.repositories.UsuarioRepository;
import java.util.Collections;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/ocorrencia")
public class OcorrenciaController {
	@Autowired
	private OcorrenciaRepository ocorrenciaRepository;
	@Autowired
	private ReputacaoRepository reputacaoRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@ApiOperation(value = "Salva uma ocorrencia", response = Ocorrencia.class)
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Ocorrencia> inserir(@RequestBody Ocorrencia ocorrencia) throws ApiException {
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.USUARIO) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		ocorrencia.setDataHora(Calendar.getInstance());
		ocorrencia.setUsuario(getCurrentUser());
		ocorrencia.setProtocolo(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()));
		return ResponseEntity.ok(ocorrenciaRepository.saveAndFlush(ocorrencia));
	}
	
	@CrossOrigin
	@ApiOperation(value = "Salva uma ocorrencia com o feedback.", response = Ocorrencia.class)
	@RequestMapping(value="/feedback", method=RequestMethod.POST)
	public ResponseEntity<Ocorrencia> feedback(@RequestBody Ocorrencia ocorrencia) throws ApiException {
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		if(ocorrencia.getId() == null) {
			return (ResponseEntity<Ocorrencia>) ResponseEntity.notFound();
		}
		Ocorrencia ocrFeedback = ocorrenciaRepository.findById(ocorrencia.getId());
		if(ocrFeedback == null || ocrFeedback.getId() == null) {
			return (ResponseEntity<Ocorrencia>) ResponseEntity.notFound();
		}
		if(ocrFeedback.getStatusOcorrencia() != StatusOcorrencia.APURACAO) {
			throw new ApiException("Ocorrência ainda não está em apuração.");
		}
		if(ocrFeedback.getUsuarioPrefeitura().getId() != getCurrentUser().getId()) {
			throw new ApiException("Ocorrência está sendo apurada por outro usuário.");
		}
		ocrFeedback.setFeedback(ocorrencia.getFeedback());
		ocrFeedback.setStatusOcorrencia(StatusOcorrencia.FEEDBACK);
		return ResponseEntity.ok(ocorrenciaRepository.saveAndFlush(ocrFeedback));
	}
	
	@CrossOrigin
	@ApiOperation(value = "Salva uma ocorrencia e muda o status para apuração.", response = Ocorrencia.class)
	@RequestMapping(value="/apurar", method=RequestMethod.POST)
	public ResponseEntity<Ocorrencia> apurar(@RequestBody Ocorrencia ocorrencia) throws ApiException {
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		if(ocorrencia.getId() == null) {
			return (ResponseEntity<Ocorrencia>) ResponseEntity.notFound();
		}
		Ocorrencia ocrFeedback = ocorrenciaRepository.findById(ocorrencia.getId());
		if(ocrFeedback == null || ocrFeedback.getId() == null) {
			return (ResponseEntity<Ocorrencia>) ResponseEntity.notFound();
		}
		if(ocrFeedback.getStatusOcorrencia() != StatusOcorrencia.ABERTA) {
			throw new ApiException("Ocorrência não está mais com o status aberta.");
		}
		ocrFeedback.setStatusOcorrencia(StatusOcorrencia.APURACAO);
		ocrFeedback.setUsuarioPrefeitura(getCurrentUser());
		return ResponseEntity.ok(ocorrenciaRepository.saveAndFlush(ocrFeedback));
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna todas ocorrencias", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/all", method=RequestMethod.GET)
	public ResponseEntity<List<Ocorrencia>> allOcorrencias(Usuario usuario) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		List<Ocorrencia> ocorrencias = ocorrenciaRepository.findAll();
		if (ocorrencias.isEmpty()){
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Ocorrencia>>(ocorrencias, HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com o status", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/byStatus/{status}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasByStatus(@PathVariable StatusOcorrencia status) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		return new ResponseEntity<Iterable<Ocorrencia>>(ocorrenciaRepository.findByStatusOcorrencia(status), HttpStatus.OK);

	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com o usuario", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/byUsuario/{usuario}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasByUsuario(@PathVariable Usuario usuario) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		return new ResponseEntity<Iterable<Ocorrencia>>(ocorrenciaRepository.findByUsuario(usuario), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com o usuario e o status", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/byUsuarioStatus/{usuario}/{status}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasByUsuarioAndStatus(@PathVariable Usuario usuario, @PathVariable StatusOcorrencia status) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		return new ResponseEntity<Iterable<Ocorrencia>>(ocorrenciaRepository.findByUsuarioAndStatusOcorrencia(usuario, status),
				HttpStatus.OK);
	}
	
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com o setor", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/bySetor/{setorId}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasBySetor(@PathVariable Long setorId) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		return new ResponseEntity<Iterable<Ocorrencia>>(ocorrenciaRepository.findBySetorId(setorId), HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com a reputação do usuário", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/orderByReputacao", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasByReputacao() throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		List<Ocorrencia> abertas = ocorrenciaRepository.findByStatusOcorrencia(StatusOcorrencia.ABERTA);
		
		Collections.sort(abertas, new Comparator<Ocorrencia>() {
	        @Override
	        public int compare(Ocorrencia oc1, Ocorrencia oc2)
	        {
	            return  reputacaoRepository.findAvgReputacaoNotaByUsuarioId(oc1.getUsuario().getId())
	            		.compareTo(reputacaoRepository.findAvgReputacaoNotaByUsuarioId(oc2.getUsuario().getId()));
	        }
	    });
		return new ResponseEntity<Iterable<Ocorrencia>>(abertas, HttpStatus.OK);
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as ocorrencias de acordo com o setor", response = Ocorrencia.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/byPeriodo/{ini}/{fim}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Ocorrencia>> allOcorrenciasByPeriodo(@PathVariable String ini, @PathVariable String fim) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try{
			
			Calendar inicio = Calendar.getInstance();
			Calendar fin = Calendar.getInstance();
			inicio.setTime(sdf.parse(ini));
			fin.setTime(sdf.parse(fim));
			
			return new ResponseEntity<Iterable<Ocorrencia>>(ocorrenciaRepository.findByPeriodo(inicio, fin), HttpStatus.OK);
		}
		catch (Exception e) {
			throw new ApiException("As datas estão com formatação errada.");
		}
		
	}
	
	public Usuario getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String nome = authentication.getName();
		return usuarioRepository.findByNome(nome);
	}
}
