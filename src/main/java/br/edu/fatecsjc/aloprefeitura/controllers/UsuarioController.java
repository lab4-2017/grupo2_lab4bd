package br.edu.fatecsjc.aloprefeitura.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.fatecsjc.aloprefeitura.exceptions.ApiException;
import br.edu.fatecsjc.aloprefeitura.models.TipoUsuario;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;
import br.edu.fatecsjc.aloprefeitura.repositories.UsuarioRepository;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/usuario")
public class UsuarioController {
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@ApiOperation(value = "Insere um usuário comum", response = Usuario.class)
	@RequestMapping(value="/inserir-cidadao", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<Usuario> inserir(@RequestBody Usuario usuario) throws ApiException {
		if(usuario.getId() == null) {
			usuario = usuarioRepository.save(usuario);
			return ResponseEntity.ok(usuario);
		} else {
			Usuario usuarioEditavel = usuarioRepository.findById(usuario.getId());
			usuarioEditavel.setNome(usuario.getNome());
			usuarioEditavel.setCpf(usuario.getCpf());
			usuarioEditavel.setEmail(usuario.getEmail());
			usuarioEditavel.setTipoUsuario(TipoUsuario.USUARIO);
			usuario = usuarioRepository.save(usuarioEditavel);
			return ResponseEntity.ok(usuario);
		}
	}
	
	@CrossOrigin
	@ApiOperation(value = "Insere um usuário da prefeitura", response = Usuario.class)
	@RequestMapping(value="/inserir-prefeitura", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<Usuario> inserirUsuarioPrefeitura(@RequestBody Usuario usuario) throws ApiException {
		if(usuario.getId() == null) {
			usuario = usuarioRepository.save(usuario);
			return ResponseEntity.ok(usuario);
		} else {
			Usuario usuarioEditavel = usuarioRepository.findById(usuario.getId());
			usuarioEditavel.setNome(usuario.getNome());
			usuarioEditavel.setEmail(usuario.getEmail());
			usuarioEditavel.setTipoUsuario(TipoUsuario.PREFEITURA);
			usuarioEditavel.setSetor(usuario.getSetor());
			usuario = usuarioRepository.save(usuarioEditavel);
			return ResponseEntity.ok(usuario);
		}
	}
	
	@CrossOrigin
	@ApiOperation(value = "Traz a lista de Usuários por setor", response = Usuario.class)
	@RequestMapping(value="/setor/{setorId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Usuario>> getUsuarioPrefeiturabySetor(@PathVariable Long setorId) throws ApiException {
			return ResponseEntity.ok(usuarioRepository.findBySetorId(setorId));
	}
}
