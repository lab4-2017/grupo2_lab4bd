package br.edu.fatecsjc.aloprefeitura.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.fatecsjc.aloprefeitura.exceptions.ApiException;
import br.edu.fatecsjc.aloprefeitura.models.Ocorrencia;
import br.edu.fatecsjc.aloprefeitura.models.Reputacao;
import br.edu.fatecsjc.aloprefeitura.models.StatusOcorrencia;
import br.edu.fatecsjc.aloprefeitura.models.TipoUsuario;
import br.edu.fatecsjc.aloprefeitura.models.Usuario;
import br.edu.fatecsjc.aloprefeitura.repositories.ReputacaoRepository;
import br.edu.fatecsjc.aloprefeitura.repositories.UsuarioRepository;
import br.edu.fatecsjc.aloprefeitura.security.MyUserDetailsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/reputacao")
public class ReputacaoController {
	@Autowired
	private ReputacaoRepository reputacaoRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@CrossOrigin
	@ApiOperation(value = "Salva a reputacao de um usuário.", response = Reputacao.class)
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Reputacao> inserir(@RequestBody Reputacao reputacao) throws ApiException {
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		reputacao.setId(null);
		if(reputacao.getUsuario() == null || reputacao.getUsuario().getId() == null) {
			throw new ApiException("Usuário não informado.");
		}
		if(reputacao.getNota() == null) {
			throw new ApiException("Informe a nota da reputação.");
		}
		if(reputacao.getNota() < 0l || reputacao.getNota() > 5l) {
			throw new ApiException("A nota da reputação deverá ser entre 0 e 5.");
		}
		return ResponseEntity.ok(reputacaoRepository.saveAndFlush(reputacao));
	}
	
	@CrossOrigin
	@ApiOperation(value = "Retorna as reputações de um usuário", response = Reputacao.class)
	@ApiResponses( value = {
			@ApiResponse(code = 200, message = "Sucesso em retornar a lista!"),
			@ApiResponse(code = 401, message = "Voce nao tem autorizacao!"),
			@ApiResponse(code = 404, message = "Nenhuma ocorrencia achada!")
	})
	@RequestMapping(value = "/show/byUsuario/{usuarioId}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Reputacao>> allReputacaoByUsuario(@PathVariable Long usuarioId) throws ApiException{
		if(getCurrentUser().getTipoUsuario() != TipoUsuario.PREFEITURA) {
			throw new ApiException("Você não pode executar essa operação.");
		}
		return new ResponseEntity<Iterable<Reputacao>>(reputacaoRepository.findByUsuarioId(usuarioId), HttpStatus.OK);
	}
	
	public Usuario getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String nome = authentication.getName();
		return usuarioRepository.findByNome(nome);
	}
}
